package com.gmail.liraan12;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



public class Parser {

    public List<File> getFileList(String directoryPath, ArrayList<File> files) {
        File directory = new File(directoryPath);
        File[] filesList = directory.listFiles();
        if (filesList != null) {
            for (File file : filesList) {
                if (file.isFile()) {

                    if (file.getName().substring(file.getName().indexOf(".")).equals(".xml")) {
                        files.add(file);
                    }
                } else if (file.isDirectory()) {
                    this.getFileList(file.getAbsolutePath(), files);
                }
            }
        }
        return files;
    }


    public void renameFiles(List<File> files) {
        if (files.size() > 0) {
            for (File file : files) {
                if (!file.getName().equals("PointOfSaleManageSvRQ_ADC.xml")) {
                    String newFileName = file.getAbsolutePath().replace("PointOfSaleManageSvRQ", "PointOfSaleManageSvRQ_ADC");
                    final File newFile = new File(newFileName);
                    file.renameTo(newFile);
                }
            }

        }
    }


    public List<File> getDirectoryList(String directoryPath, ArrayList<File> folders) {
        File directory = new File(directoryPath);
        File[] filesList = directory.listFiles();
        if (filesList != null) {
            for (File folder : filesList) {
                if (folder.isDirectory()) {
                    folders.add(folder);
                    this.getDirectoryList(folder.getAbsolutePath(), folders);
                }
            }
        }
        folders.sort((o1, o2) -> o2.getAbsolutePath().compareTo(o1.getAbsolutePath()));
        return folders;
    }


    public void renameDirectories(String directoryPath, List<File> folders) {
        if (folders.size() > 0) {
            for (File folder : folders) {
                String path = folder.getAbsolutePath();
                path = path.replace(folder.getName(), "ADC_" + folder.getName());
               folder.renameTo(new File(path));
            }
        }

    }


    public void renameAttributes(List<File> files) {
        if (files.size() > 0) {
            for (File file : files) {
                if (!file.getName().equals("PointOfSaleManageSvRQ_ADC.xml")) {
                    final String[] splitElement = file.getAbsolutePath().split("\\\\");
                    try {
                        String filepath = file.getAbsolutePath();
                        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
                        Document doc = docBuilder.parse(filepath);
                        final Node pointOfSale = doc.getElementsByTagName("PointOfSale").item(0);
                        pointOfSale.getAttributes().getNamedItem("ParentPointOfSale").setNodeValue(splitElement[splitElement.length - 3]);
                        pointOfSale.getAttributes().getNamedItem("PointOfSaleCode").setNodeValue(splitElement[splitElement.length - 2]);

                        NodeList list = doc.getElementsByTagName("PointOfSaleDescription");
                        if (list != null) {
                            final Node pointOfSaleDescription = list.item(0);
                            if (pointOfSaleDescription != null) {
                                pointOfSaleDescription.getAttributes().getNamedItem("Description").setNodeValue(splitElement[splitElement.length - 2]);
                            }
                        }
                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
                        Transformer transformer = transformerFactory.newTransformer();
                        DOMSource source = new DOMSource(doc);
                        StreamResult result = new StreamResult(new File(filepath));
                        transformer.transform(source, result);
                    } catch (final ParserConfigurationException | TransformerException | SAXException | IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        }
    }
}