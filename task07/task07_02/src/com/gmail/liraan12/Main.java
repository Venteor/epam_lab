package com.gmail.liraan12;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static void main(String[] args) {

        File sourcePath = new File("ADC\\");
        File newPath = new File("changedFolder\\ADC\\");//каталог назначения

        try {
            FileUtils.copyDirectory(sourcePath, newPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parser parser = new Parser();

        List<File> Files = parser.getFileList(newPath.toString(), new ArrayList<>());

        parser.renameFiles(Files);

        List<File> Directories = parser.getDirectoryList(newPath.toString(), new ArrayList<>());

        parser.renameDirectories(newPath.toString(), Directories);

        List<File> newArrayFiles = parser.getFileList(newPath.toString(), new ArrayList<>());

        parser.renameAttributes(newArrayFiles);
    }
}
