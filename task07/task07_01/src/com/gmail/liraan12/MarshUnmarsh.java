package com.gmail.liraan12;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class MarshUnmarsh {
    public static void main(String[] args) {
        String fileName = "e:\\EPAM\\repositories\\epam_lab\\task07\\task07_01\\flaWareMarsh.xml";
        FlatWare flatWare = new FlatWare();
        flatWare.setType("Knife");
        flatWare.setOrigin("Russia");
        flatWare.setLength(150);
        flatWare.setValue("Collectible");

        convertObjectToXml(flatWare, fileName);

        // восстанавливаем объект из XML файла
        FlatWare unmarshFlatware = fromXmlToObject(fileName);
        if (unmarshFlatware != null) {
            System.out.println(unmarshFlatware.toString());
        }
    }


    private static FlatWare fromXmlToObject(String filePath) {
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(FlatWare.class);
            Unmarshaller un = jaxbContext.createUnmarshaller();

            return (FlatWare) un.unmarshal(new File(filePath));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void convertObjectToXml(FlatWare flatWare, String filePath) {
        try {
            JAXBContext context = JAXBContext.newInstance(FlatWare.class);
            Marshaller marshaller = context.createMarshaller();
            // устанавливаем флаг для читабельного вывода XML в JAXB
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // маршаллинг объекта в файл
            marshaller.marshal(flatWare, new File(filePath));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

}
