package com.gmail.liraan12;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "flatWare")

@XmlType(propOrder = {"type", "origin", "length", "value"})

public class FlatWare {
    private String type;
    private String origin;
    private int length;
    private String value;

    public FlatWare(){
    }
    public FlatWare (String type, String origin, int length, String value){
        this.type = type;
        this.origin = origin;
        this.type = type;
        this.value = value;
    }

    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }

    public String getOrigin() {
        return origin;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }
    @Override
    public String toString() {
        return "FlatWare{" +
                "type = '" + type + '\'' +
                ", origin = '" + origin + '\'' +
                ", length = " + length +
                ", value = '" + value + '\'' +
                '}';
    }
}
