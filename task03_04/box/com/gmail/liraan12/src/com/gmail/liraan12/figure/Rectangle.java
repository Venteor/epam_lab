package com.gmail.liraan12.figure;

public abstract class Rectangle extends Figure {
    private double width, height;
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    public double getWidth() {
        return width;
    }
    public double getHeight() {
        return height;
    }
    public void setWidth (double width) {
        this.width = width;
    }
    public void setHeight (double height) {
        this.height = height;
    }
    @Override
    public double getArea() {
        return width*height;
    }
    public double getPerimeter() {
        return 2*width+2*height;
    }
    @Override
    public String toString() {
        return super.toString() + ": width = " + width +"; height = " + height;
    }
}
