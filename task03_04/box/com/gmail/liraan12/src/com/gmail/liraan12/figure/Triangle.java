package com.gmail.liraan12.figure;

public abstract class Triangle extends Figure {
    private double side;
    public Triangle(double side) {
        this.side = side;
    }
    public double getSide() {
        return side;
    }
    public void setSide (double side) {
        this.side = side;
    }
    @Override
    public double getArea() {
        return Math.sqrt(3)/4*side*side;
    }
    public double getPerimeter() {
        return 3*side;
    }
    @Override
    public String toString() {
        return super.toString() + ": side = " + side;
    }
}
