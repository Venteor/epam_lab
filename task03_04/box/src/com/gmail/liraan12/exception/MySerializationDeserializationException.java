/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.exception;

public class MySerializationDeserializationException extends RuntimeException {
    public  MySerializationDeserializationException(){
        super();
    }
    public  MySerializationDeserializationException(String message){
        super(message);
    }
    public MySerializationDeserializationException(Throwable cause){
        super(cause);
    }
    public MySerializationDeserializationException(String message, Throwable cause){
        super(message, cause);
    }
}
