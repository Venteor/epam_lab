/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.exception;

public class NullException extends Exception {
    public NullException(){
        super();
    }
    public NullException(String message){
        super(message);
    }
}
