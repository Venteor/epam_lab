/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.exception;

public class NegativeValueException extends Exception{
    private static final long serialVersionUID = 1L;
    public NegativeValueException(){
            super();
        }
        public NegativeValueException(String message){
            super(message);
        }

}
