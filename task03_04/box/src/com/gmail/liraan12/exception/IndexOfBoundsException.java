/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.exception;

public class IndexOfBoundsException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public IndexOfBoundsException() {
        super();
    }
    public IndexOfBoundsException(String message){
        super(message);
    }
}
