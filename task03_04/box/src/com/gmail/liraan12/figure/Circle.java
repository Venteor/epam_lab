/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.figure;

import java.util.Objects;

/**
 * Provides abstraction for PaperCircle, FilmCircle.
 * Calculate Area, Perimeter, Minimal size for cut.
 */

public abstract class Circle extends Figure {
    private static final long serialVersionUID = 1L;
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
    return radius;
    }
    public void setRadius (double radius) {
        this.radius = radius;
    }

    /**
     * Calculate minimal size for carved Circle.
     * @param figure
     */

    public Circle(Figure figure){
        this.radius = figure.getMinSize()/2;
    }

    /**
     * @return radius cutting Circle.
     */

    @Override
    public double getMinSize(){
        return radius;
    }

    /**
     * Calculate area.
     * @return area of Circle.
     */

    @Override
    public double getArea() {
        return Math.PI*radius*radius;
    }

    /**
     * Calculate perimeter.
     * @return perimeter of Circle.
     */

    @Override
    public double getPerimeter() {
        return Math.PI*2*radius;
    }

    /**
     * @return string consist of name class and it's radius.
     */

    @Override
    public String toString() {
        return super.toString() + ": radius = " + radius;
    }

    /**
     * Equality check.
     * @param o
     * @return true if o equals figure and false if not.
     */

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!super.equals(0)) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0;
    }

    /**
     * @return hashcode of Circle by radius.
     */

    @Override
    public int hashCode(){
        return Objects.hash(radius);
    }
}

