/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.figure;

import java.util.Objects;

/**
 * Provides abstraction for PaperRectangle, FilmRectangle.
 * Calculate Area, Perimeter, Minimal size for cut.
 */

public abstract class Rectangle extends Figure {
    transient double width;
    private static final long serialVersionUID = 1L;
    private static double height;
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    public double getWidth() {
        return width;
    }
    public double getHeight() {
        return height;
    }
    public void setWidth (double width) {
        this.width = width;
    }
    public void setHeight (double height) {
        this.height = height;
    }

    /**
     * Calculate minimal size for carved Rectangle.
     * @param figure
     */

    public Rectangle(Figure figure){
        this.height = figure.getMinSize()/2;
        this.width = height;
    }

    /**
     * @return radius cutting Rectangle.
     */

    @Override
    public double getMinSize(){
        return Math.min(height,width);
    }

    /**
     * Calculate area.
     * @return area of Rectangle.
     */

    @Override
    public double getArea() {
        return width*height;
    }

    /**
     * Calculate perimeter.
     * @return perimeter of Rectangle.
     */

    @Override
    public double getPerimeter() {
        return 2*width+2*height;
    }

    /**
     * @return string consist of name class and it's width and height.
     */

    @Override
    public String toString() {
        return super.toString() + ": width = " + width +"; height = " + height;
    }

    /**
     * Equality check.
     * @param o
     * @return true if o equals figure and false if not.
     */

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!super.equals(0)) return false;
        Rectangle rectangle = (Rectangle) o;
        return Double.compare(rectangle.height, height) == 0 && Double.compare(rectangle.width, width)==0;
    }

    /**
     * @return hashcode of Rectangle by height and width.
     */

    @Override
    public int hashCode(){
        return Objects.hash(height, width);
    }
}
