/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.figure;

import java.util.Objects;

/**
 * Provides abstraction for PaperTriangle, FilmTriangle.
 * Calculate Area, Perimeter, Minimal size for cut.
 */

public abstract class Triangle extends Figure {
    private static final long serialVersionUID = 1L;
    private double side;
    public Triangle(double side) {
        this.side = side;
    }
    public double getSide() {
        return side;
    }
    public void setSide (double side) {
        this.side = side;
    }

    /**
     * Calculate minimal size for carved Circle.
     * @param figure
     */

    public Triangle(Figure figure){
        this.side = figure.getMinSize()-1;
    }

    /**
     * @return radius cutting Triangle.
     */

    @Override
    public double getMinSize(){
        return side;
    }

    /**
     * Calculate area.
     * @return area of Triangle.
     */

    @Override
    public double getArea() {
        return Math.sqrt(3)/4*side*side;
    }

    /**
     * Calculate perimeter.
     * @return perimeter of Triangle.
     */

    @Override
    public double getPerimeter() {
        return 3*side;
    }

    /**
     * @return string consist of name class and it's side.
     */

    @Override
    public String toString() {
        return super.toString() + ": side = " + side;
    }

    /**
     * Equality check.
     * @param o
     * @return true if o equals figure and false if not.
     */

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!super.equals(0)) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.side, side) == 0;
    }

    /**
     * @return hashcode of Triangle by side.
     */

    @Override
    public int hashCode(){
        return Objects.hash(side);
    }
}
