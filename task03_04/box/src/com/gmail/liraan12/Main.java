/**
 * @author Trukhan AD on 01.05.2019
 */

package com.gmail.liraan12;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.System;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.util.ArrayList;
import java.util.List;


import com.gmail.liraan12.connector.Connector;
import com.gmail.liraan12.exception.MySerializationDeserializationException;
import com.gmail.liraan12.exception.NegativeValueException;
import com.gmail.liraan12.exception.NullException;
import com.gmail.liraan12.figure.*;
import com.gmail.liraan12.box.*;
import com.gmail.liraan12.film.*;
import com.gmail.liraan12.paper.*;

/**
 * Class for test Box
 */


public class Main {
    private static final long serialVersionUID = 1L;
    public static void main(String args[]){
        String fileName = "file.txt";
        Box box = new Box();
        PaperCircle paperCircle1 = new PaperCircle(1);
        FilmCircle filmCircle2 = new FilmCircle(20);
        PaperCircle paperCircle3 = new PaperCircle(3);
        PaperCircle paperCircle4 = new PaperCircle(30);
        paperCircle1.setColor(Paper.Color.BlUE);
        PaperCircle paperCircle5 = new PaperCircle(paperCircle3);
        FilmCircle filmCircle6 = new FilmCircle(filmCircle2);
        paperCircle5.setColor(Paper.Color.RED);
        paperCircle5.setColor(Paper.Color.BLACK);
        /*
        System.out.println(paperCircle1.toString() + "; perimeter: " + paperCircle1.getPerimeter() + "; area: " + paperCircle1.getArea());
        System.out.println(paperCircle4.toString() + "; perimeter: " + paperCircle4.getPerimeter() + "; area: " + paperCircle4.getArea());
        System.out.println(paperCircle5.toString() + "; perimeter: " + paperCircle5.getPerimeter() + "; area: " + paperCircle5.getArea());
        System.out.println(filmCircle6.toString() + "; perimeter: " + filmCircle6.getPerimeter() + "; area: " + filmCircle6.getArea());
        */

        FilmRectangle filmRectangle1 = new FilmRectangle(1,2);
        FilmRectangle filmRectangle2 = new FilmRectangle(2,3);
        PaperRectangle paperRectangle3 = new PaperRectangle(3,4);
        PaperRectangle paperRectangle4 = new PaperRectangle(paperRectangle3);
        paperRectangle4.setColor(Paper.Color.GREEN);
        paperRectangle4.setColor(Paper.Color.BLACK);
        /*
        System.out.println(filmRectangle1.toString() + "; perimeter: " + filmRectangle1.getPerimeter() + "; area: " + filmRectangle1.getArea());
        System.out.println(filmRectangle2.toString() + "; perimeter: " + filmRectangle2.getPerimeter() + "; area: " + filmRectangle2.getArea());
        System.out.println(paperRectangle4.toString() + "; perimeter: " + paperRectangle4.getPerimeter() + "; area: " + paperRectangle4.getArea());
        */

        PaperTriangle paperTriangle1 = new PaperTriangle(20);
        PaperTriangle paperTriangle2 = new PaperTriangle(500);
        PaperTriangle paperTriangle3 = new PaperTriangle(40);
        FilmTriangle filmTriangle5 = new FilmTriangle(4);
        paperTriangle1.setColor(Paper.Color.WHITE);
        paperTriangle2.setColor(Paper.Color.BLACK);
        PaperTriangle paperTriangle4 = new PaperTriangle(paperTriangle3);
        paperTriangle4.setColor(Paper.Color.GREEN);
        paperTriangle4.setColor(Paper.Color.BLACK);
        /*
        System.out.println(paperTriangle1.toString() + "; perimeter: " + paperTriangle1.getPerimeter() + "; area: " + paperTriangle1.getArea());
        System.out.println(paperTriangle2.toString() + "; perimeter: " + paperTriangle2.getPerimeter() + "; area: " + paperTriangle2.getArea());
        System.out.println(paperTriangle4.toString() + "; perimeter: " + paperTriangle4.getPerimeter() + "; area: " + paperTriangle4.getArea());
        */

        try{
            box.addFigure(paperCircle1);
            box.addFigure(paperCircle5);
            box.addFigure(paperCircle1);
            box.addFigure(filmRectangle1);
            box.addFigure(paperTriangle1);
            box.addFigure(paperTriangle2);
            box.addFigure(paperTriangle4);
        } catch (NullException e){
            System.out.println("It is not a figure");
        }
        try{
            box.setFigure(3, filmCircle6);
        } catch (NegativeValueException e) {
            System.out.println("Negative index");
        }
        try {
            box.addFigure(-3, filmTriangle5);
        } catch (NegativeValueException e){
            System.out.println("Negative index");
        } catch (NullException e){
            System.out.println("It is not a figure");
        }
        try{
            box.removeFigure(0);
        } catch (NegativeValueException e){
            System.out.println("Negative index");
        }
        /*
        for (int i = 0; i < box.getCount(); i++){
            try{
                System.out.println(box.getFigure(i));
            } catch (NegativeValueException e){
                System.out.println("Negative index");
            }
        }
        List<Figure> box1 = new ArrayList<>();
        box1.add(filmRectangle2);
        box1.add(paperRectangle4);
        box1.add(paperTriangle1);
        try{
            box.addAllFigure(0, box1);
        } catch (NegativeValueException e){
            System.out.println("Negative index");
        } catch (NullException e){
            System.out.println("It is not a figure");
        }
        System.out.println("Box + Box1:");
        for (int i = 0; i < box.getCount(); i++){
            try{
                System.out.println(box.getFigure(i));
            } catch (NegativeValueException e){
                System.out.println("Negative index");
            }
        }
        System.out.println("Count of figures in Box: " + box.getCount());
        System.out.println("Area of all figures in Box: " + box.getArea());
        System.out.println("Perimeter of all figures in Box: " + box.getPerimeter());
        try {
            System.out.println("The Box has red circle radius 1.5 - " + box.findByFigure(paperCircle5));
            System.out.println("The Box has blue circle radius 1 - " + box.findByFigure(paperCircle1));
        } catch (NullException e){
            System.out.println("It is not a figure");
        }
        System.out.println("All Circles from the Box:");
        ArrayList<Figure> boxCircle = box.getFigure(Circle.class);
        for (int i = 0; i < boxCircle.size(); i++){
            System.out.println(boxCircle.get(i));
        }
        System.out.println("All Film figures from the Box:");
        ArrayList<Figure> boxFilm = box.getFigure(Film.class);
        for (int i = 0; i < boxFilm.size(); i++){
            System.out.println(boxFilm.get(i));
        }
        System.out.println("All Red Circles from the Box:");
        ArrayList<Figure> boxRedCircle = box.getRedCircle();
        for (int i = 0; i < boxRedCircle.size(); i++){
            System.out.println(boxRedCircle.get(i));
        }
        List<Figure> sublist = new ArrayList<>();
        sublist = box.getAllFigureByIndex(1,6);
        System.out.println("Sublist from the Box:");
        for (int i = 0; i < sublist.size(); i++){
            System.out.println(sublist.get(i));
        }
        box.sortByColor();
        System.out.println(box.toString());
        box.sortByArea();
        System.out.println(box.toString());
        */
        System.out.println(box.toString());

        try{
            Connector.writeBox(fileName, box);
        } catch (MySerializationDeserializationException e) {
            if(e.getCause() instanceof FileNotFoundException){
                e.printStackTrace();
            }
            if(e.getCause() instanceof IOException){
                e.printStackTrace();
            }
        }
        Box desertBox = new Box();
        try {
            desertBox = Connector.readBox(fileName);
        } catch (MySerializationDeserializationException e){
            if(e.getCause() instanceof ClassNotFoundException) e.printStackTrace();
            if(e.getCause() instanceof FileNotFoundException) e.printStackTrace();
            if(e.getCause() instanceof NoSuchFieldException) e.printStackTrace();
            if(e.getCause() instanceof NullException) e.printStackTrace();
            if(e.getCause() instanceof IOException) e.printStackTrace();
        }
        System.out.println(desertBox.toString());

    }
}
