/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.paper;

import com.gmail.liraan12.figure.Figure;
import com.gmail.liraan12.figure.Rectangle;

import java.util.Objects;

/**
 * Provides creating PaperRectangle objects.
 */

public class PaperRectangle extends Rectangle implements Paper {
    private static final long serialVersionUID = 1L;
    private Painted painted = new Painted();
    public PaperRectangle (double height, double width) {
        super(height, width);
    }
    public PaperRectangle (Paper paperFigure){
        super((Figure)paperFigure);
        painted = paperFigure.getPainted();
    }

    /**
     * @return color of object.
     */

    @Override
    public Color getColor(){
        return painted.getColor();
    }

    /**
     * Paints object in color.
     * @param color
     */

    @Override
    public void setColor (Color color){
        painted.setColor(color);
    }

    /**
     * @return painted object
     */

    @Override
    public Painted getPainted(){
        return painted;
    }

    /**
     * @return string consist of name class, it's height and width and color.
     */

    @Override
    public String toString(){
        return super.toString() + ", painted = " + painted.getColor();
    }

    /**
     * Equality check.
     * @param o
     * @return true if o equals figure and false if not.
     */

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!super.equals(o)) return false;
        PaperRectangle paperRectangle = (PaperRectangle) o;
        return Objects.equals(paperRectangle.painted, painted);
    }

    /**
     * @return hashcode of Rectangle by painted.
     */

    @Override
    public int hashCode(){
        return Objects.hash(painted);
    }
}