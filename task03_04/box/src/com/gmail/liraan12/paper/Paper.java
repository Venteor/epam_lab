/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.paper;

import com.gmail.liraan12.figure.Figure;

import java.io.Serializable;

/**
 * Provides creating Paper objects.
 * Has enum Color.
 */

public interface Paper {
    enum Color {
        RED, WHITE, GREEN, BlUE, BLACK;
    }

    /**
     * Provides painting Paper objects.
     */

    class Painted implements Serializable {
        private static final long serialVersionUID = 1L;
        private Color color;
        public Color getColor(){
            return color;
        }
        public void setColor(Color color){
            if(this.color==null){
                this.color=color;
            }
        }
    }
    Color getColor();
    void setColor(Color color);
    Painted getPainted();
}


