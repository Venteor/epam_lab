/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.box;
/**
 * @author Trukhan AD on 16.04.2019
 */

import com.gmail.liraan12.figure.Figure;
import com.gmail.liraan12.paper.Paper;

import java.util.Comparator;

/**
 * Compare figures in the box.
 */

public class ComparatorFigure implements Comparator <Figure> {
    private static final long serialVersionUID = 1L;

    /**
     * Compare figures by shape using names without type of material.
     * @param o1 first figure
     * @param o2 second figure
     * @return 1 if o1 > o2; -1 if o1 < o2;  0 if o1 == o2;
     */
    @Override
    public int compare(Figure o1, Figure o2){
        return o1.getClass().getSuperclass().getSimpleName().compareTo(o2.getClass().getSuperclass().getSimpleName());
    }
    /**
     * Inner class.
     * Compare figures in the box.
     */
    public static class InnerComparator implements Comparator <Figure>{
        private static final long serialVersionUID = 1L;
        /**
         * Compare figures by color using compareTo for String.
         * @param o1 first figure
         * @param o2 second figure
         *
         */
        @Override
        public int compare(Figure o1, Figure o2){
            String color1 = getColor(o1);
            String color2 = getColor(o2);
            return color1.compareTo(color2);
        }
        /**
         * @param o figure frrom box
         * @return name color figure o
         */
        private String getColor(Figure o) {
            if (o instanceof Paper && ((Paper) o).getColor() != null) {
                return ((Paper) o).getColor().name();
            }
            return "";
        }
    }

}
