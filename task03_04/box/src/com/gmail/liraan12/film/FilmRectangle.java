/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.film;

import com.gmail.liraan12.figure.Rectangle;
import com.gmail.liraan12.figure.Figure;

/**
 * Provides creating FilmRectangle objects.
 */

public class FilmRectangle extends Rectangle implements Film{
    private static final long serialVersionUID = 1L;
    public FilmRectangle (double width, double height){
        super(height, width);
    }
    public FilmRectangle(Film filmFigure){
        super((Figure)filmFigure);
    }
}