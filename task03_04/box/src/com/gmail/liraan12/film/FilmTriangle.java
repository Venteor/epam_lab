/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.film;

import com.gmail.liraan12.figure.Triangle;
import com.gmail.liraan12.figure.Figure;

/**
 * Provides creating FilmTriangle objects.
 */

public class FilmTriangle extends Triangle implements Film{
    private static final long serialVersionUID = 1L;
    public FilmTriangle (double side){
        super(side);
    }
    public FilmTriangle(Film filmFigure){
        super((Figure)filmFigure);
    }
}
