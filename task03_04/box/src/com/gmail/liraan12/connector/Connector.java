/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.connector;

import com.gmail.liraan12.box.Box;
import com.gmail.liraan12.exception.MySerializationDeserializationException;
import com.gmail.liraan12.exception.NullException;
import com.gmail.liraan12.figure.Figure;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;


public class Connector {
    /**
     * @param fileName - name of file for serialization
     * @param box - Box for serialization
     * Write Box on PC
     */
    public static void writeBox(String fileName, Box box) throws MySerializationDeserializationException{
        Path path = Paths.get(fileName);
        try(ObjectOutputStream oos = new ObjectOutputStream((Files.newOutputStream(path)))){
        for(Figure figure : box.getFigureList()){
            oos.writeObject(figure);
            }
        } catch (FileNotFoundException e){
            throw new MySerializationDeserializationException("File " + fileName + " was not found", e);
        } catch (IOException e){
            throw new MySerializationDeserializationException("There was an input/output error", e);
        }
    }
    /**
     * @param fileName - name of file for deserialization
     * @return deserBox - deserialization Box from PC
     * Read Box from PC
     */
    public static Box readBox(String fileName) throws MySerializationDeserializationException{
        Box deserBox = new Box();
        Object figure = null;
        Path path = Paths.get(fileName);
        try(ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(path))){
            while ((figure = ois.readObject()) != null){
                if(figure instanceof Figure) {
                    deserBox.addFigure((Figure) figure);
                }
            }
        } catch (EOFException e) {
            return deserBox;
        } catch (ClassNotFoundException e) {
            throw new MySerializationDeserializationException("Class not found", e);
        } catch(FileNotFoundException e) {
            throw new MySerializationDeserializationException("File " + fileName + " was not found", e);
        } catch (NoSuchFileException e){
            throw new MySerializationDeserializationException("File " + fileName + " was not found", e);
        } catch (NullException e) {
            throw new MySerializationDeserializationException("Don't try use null", e);
        } catch (IOException e){
            throw new MySerializationDeserializationException("There was input/output error", e);
        }
        return deserBox;
    }
}
