package com.gmail.liraan12.servlet;

import com.gmail.liraan12.entity.ToDo;
import com.gmail.liraan12.entity.User;
import com.gmail.liraan12.service.ToDoService;
import com.gmail.liraan12.service.UploadFile;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;


import java.sql.Date;
import java.util.List;


@WebServlet("/todo")
@MultipartConfig
public class ToDoServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String navigate = request.getParameter("navigation");

        if(request.getParameter("add") != null) add(request,response);
        if(request.getParameter("delete") != null) delete(request,response);
        if(request.getParameter("execute") != null) execute(request,response);
        if(request.getParameter("Delete File") != null) deleteFile(request,response);
        if(navigate != null){
            LocalDate today = LocalDate.now();
            LocalDate tomorrow = today.plusDays(1);
            switch (navigate){
                case "allTasks":
                    post(request, response);
                    System.out.println(navigate);
                    break;
                case "today":
                    Date todaySQL = Date.valueOf(today);
                    post(request,response,todaySQL);
                    System.out.println(navigate);
                    break;
                case "tomorrow":
                    Date tomorrowSQL = Date.valueOf(tomorrow);
                    post(request,response,tomorrowSQL);
                    System.out.println(navigate);
                    break;
                case "someday":
                    Date todSQL = Date.valueOf(today);
                    Date tomSQL = Date.valueOf(tomorrow);
                    post(request,response,todSQL, tomSQL);
                    break;
                case "recycleBin":
                    request.getRequestDispatcher("/recycleBin").forward(request, response);
                    break;
            }
        }

    }

    private void add (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ToDo toDo = new ToDo();
        ToDoService toDoService = new ToDoService();

        response.setContentType("text/html");
        int userId = Integer.parseInt(request.getParameter("userId"));
        String task = request.getParameter("task");
        LocalDate taskDate = LocalDate.parse(request.getParameter("date"));
        Date date = Date.valueOf(taskDate);


        Part file = request.getPart("file");
        UploadFile uploadFile = new UploadFile();
        String[] file_and_path;
        if(file.getSize() > 0) {
            file_and_path = uploadFile.uploadFile(file, userId);
            toDo.setFileOrigNameToDo(file.getSubmittedFileName());
            toDo.setFileNameToDo(file_and_path[0]);
            toDo.setPathFile(file_and_path[1]);
        }

        System.out.println(date + " " + "add");

        toDo.setUserId(userId);
        toDo.setDateToDo(date);
        toDo.setNameToDo(task);
        toDo.setStatusToDo((byte)1);


        try {
            toDoService.add(toDo);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        if(!taskDate.isEqual(LocalDate.now()) && !taskDate.isEqual(LocalDate.now().plusDays(1))){
        post(request,response);
        }
        else post(request, response, date);
    }

    private void delete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String[] checkedTask = request.getParameterValues("idtask");
        ToDo toDo = new ToDo();
        ToDoService toDoService = new ToDoService();
        if (checkedTask != null){
            try {
                for(String str : checkedTask){
                    int idtask = Integer.parseInt(str);
                    toDo = toDoService.getById(idtask);
                    toDo.setStatusToDo((byte) 0);
                    toDoService.update(toDo);
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        post(request,response);
    }

    private void execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] checkedTask = request.getParameterValues("idtask");
        ToDo toDo = new ToDo();
        ToDoService toDoService = new ToDoService();


        if (checkedTask != null){
            try {
                for(String str : checkedTask){
                    System.out.println(str);
                    int idtask = Integer.parseInt(str);
                    toDo = toDoService.getById(idtask);
                    toDo.setStatusToDo((byte) 2);
                    toDoService.update(toDo);
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } else {
            System.out.println("Rubbish");
        }
        post(request,response);
    }

    private void deleteFile(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ToDo toDo = new ToDo();
        ToDoService toDoService = new ToDoService();
        int idtask = Integer.parseInt(request.getParameter("Delete File"));
        try {
            toDo = toDoService.getById(idtask);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        UploadFile deleteFile = new UploadFile();
        if(deleteFile.deleteFile(toDo.getFileNameToDo(), toDo.getPathFile())){
            toDo.setPathFile(null);
            toDo.setFileOrigNameToDo(null);
            toDo.setFileNameToDo(null);
        }
        try {
            toDoService.update(toDo);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        post(request,response);

    }



    private void post(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ToDoService toDoService = new ToDoService();
        List<ToDo> toDoList = null;
        User user = new User();

        HttpSession session = request.getSession();
        user = (User)session.getAttribute("user");
        int userId = user.getId();

        try {
            toDoList = toDoService.getAllByIdActive(userId);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        request.setAttribute("todolist", toDoList);
        request.getRequestDispatcher("jsp/user.jsp").forward(request, response);
    }

    private void post(HttpServletRequest request, HttpServletResponse response, Date date)
            throws ServletException, IOException {
        ToDoService toDoService = new ToDoService();
        List<ToDo> toDoList = null;
        User user = new User();

        HttpSession session = request.getSession();
        user = (User)session.getAttribute("user");
        int userId = user.getId();

        try {
            toDoList = toDoService.getAllByIdActive(userId, date);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        request.setAttribute("todolist", toDoList);
        request.getRequestDispatcher("jsp/user.jsp").forward(request, response);
    }

    private void post(HttpServletRequest request, HttpServletResponse response, Date today, Date tomorrow)
            throws ServletException, IOException {
        ToDoService toDoService = new ToDoService();
        List<ToDo> toDoList = null;
        User user = new User();

        HttpSession session = request.getSession();
        user = (User)session.getAttribute("user");
        int userId = user.getId();

        try {
            toDoList = toDoService.getAllByIdActive(userId, today, tomorrow);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        request.setAttribute("todolist", toDoList);
        request.getRequestDispatcher("jsp/user.jsp").forward(request, response);
    }


}