package com.gmail.liraan12.servlet;

import com.gmail.liraan12.entity.ToDo;
import com.gmail.liraan12.entity.User;
import com.gmail.liraan12.service.ToDoService;
import com.gmail.liraan12.service.UploadFile;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;


@WebServlet("/recycleBin")

public class RecycleServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String delete = request.getParameter("delete");
        String reburn = request.getParameter("reburn");
        String deleteAll = request.getParameter("deleteAll");

        if(delete != null) delete(request,response);
        if(reburn != null) reburn(request,response);
        if(deleteAll != null) deleteAll(request,response);

        post(request,response);

    }

    private void delete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String[] checkedTask = request.getParameterValues("idtask");
        ToDo toDo = new ToDo();
        ToDoService toDoService = new ToDoService();
        if (checkedTask != null){
            try {
                for(String str : checkedTask){
                    int idtask = Integer.parseInt(str);
                    toDo = toDoService.getById(idtask);
                    if(toDo.getFileNameToDo() != null){
                        UploadFile deleteFile = new UploadFile();
                        deleteFile.deleteFile(toDo.getFileNameToDo(), toDo.getPathFile());}
                    toDoService.remove(toDo);
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        post(request,response);
    }

    private void deleteAll(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ToDoService toDoService = new ToDoService();
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute("user");
        List<ToDo> toDoList = null;

        try {
            toDoList = toDoService.getAllByIdDeleted(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(toDoList != null){
            try {
                for(ToDo toDo : toDoList){
                    if(toDo.getFileNameToDo() != null){
                        UploadFile deleteFile = new UploadFile();
                        deleteFile.deleteFile(toDo.getFileNameToDo(), toDo.getPathFile());};
                    toDoService.remove(toDo);
                }

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        post(request,response);
    }

    private void reburn(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] checkedTask = request.getParameterValues("idtask");
        ToDo toDo = new ToDo();
        ToDoService toDoService = new ToDoService();
        LocalDate today = LocalDate.now();
        Date todaySQL = Date.valueOf(today);
        if (checkedTask != null){
        try {
            for(String str : checkedTask){
                int idtask = Integer.parseInt(str);
                toDo = toDoService.getById(idtask);
                toDo.setStatusToDo((byte) 1);
                toDo.setDateToDo(todaySQL);
                toDoService.update(toDo);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        } else {
            System.out.println("Rubbish");
        }
        post(request,response);
    }

    private void post(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ToDoService toDoService = new ToDoService();
        List<ToDo> toDoList = null;
        User user = new User();


        HttpSession session = request.getSession();
        user = (User)session.getAttribute("user");
        int userId = user.getId();

        try {
            toDoList = toDoService.getAllByIdDeleted(userId);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        request.setAttribute("todolist", toDoList);
        request.getRequestDispatcher("jsp/recycleBin.jsp").forward(request, response);
    }


}