package com.gmail.liraan12.servlet;

import com.gmail.liraan12.entity.ToDo;
import com.gmail.liraan12.entity.User;
import com.gmail.liraan12.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/signup")
public class SignUpServlet extends HttpServlet {

    boolean b = true;

    UserService userService = new UserService();
    User user = new User();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");

        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        user.setId(user.hashCode());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setLogin(login);
        user.setPassword(password);
        try {
            b = userService.add(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (b) {
            request.setAttribute("firstname", firstName);
            request.getRequestDispatcher("jsp/congratulation.jsp").forward(request, response);
        } else {
            request.setAttribute("error","Something wrong. Please, try again.");
            request.setAttribute("previouspage","registration.html");
            request.getRequestDispatcher("jsp/error.jsp").forward(request, response);
        }


    }
}
