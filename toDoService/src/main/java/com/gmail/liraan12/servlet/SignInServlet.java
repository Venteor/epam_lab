package com.gmail.liraan12.servlet;

import com.gmail.liraan12.entity.ToDo;
import com.gmail.liraan12.entity.User;
import com.gmail.liraan12.service.ToDoService;
import com.gmail.liraan12.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/signin")
public class SignInServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<ToDo> toDoList = null;
        User user = null;

        UserService userService = new UserService();
        ToDoService toDoService = new ToDoService();

        HttpSession session = request.getSession();

        response.setContentType("text/html");

        //name not null
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        try {
            user = userService.getByLoginPassword(login, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(user != null) {
            try {
                toDoList = toDoService.getAllByIdActive(user.getId());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            session.setAttribute("user", user);
            request.setAttribute("todolist", toDoList);
            request.getRequestDispatcher("jsp/user.jsp").forward(request, response);
//            String path = request.getContextPath() + "/jsp/user.jsp";
//            response.sendRedirect(path);
        } else {
            request.setAttribute("error","User not found or wrong password");
            request.setAttribute("previouspage","login.html");
            request.getRequestDispatcher("jsp/error.jsp").forward(request, response);
        }
    }
}
