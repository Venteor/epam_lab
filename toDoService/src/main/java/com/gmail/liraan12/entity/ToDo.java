package com.gmail.liraan12.entity;

import java.sql.Date;

public class ToDo {
    private Integer id;
    private Date dateToDo;
    private String nameToDo;
    private Byte statusToDo;
    private String fileOrigNameToDo;
    private String filePathToDo;
    private String fileNameToDo;
    private Integer userId;

    public ToDo() {

    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public Date getDateToDo() {
        return dateToDo;
    }

    public void setDateToDo(Date dateToDo) {
        this.dateToDo = dateToDo;

    }

    public String getNameToDo() {
        return nameToDo;
    }

    public void setNameToDo(String nameToDo) {
        this.nameToDo = nameToDo;
    }

    public Byte getStatusToDo() {
        return statusToDo;
    }

    public void setStatusToDo(Byte statusToDo) {
        this.statusToDo = statusToDo;
    }

    public String getFileOrigNameToDo() {
        return fileOrigNameToDo;
    }

    public void setFileOrigNameToDo(String fileOrigNameToDo) {
        this.fileOrigNameToDo = fileOrigNameToDo;
    }

    public String getFileNameToDo() {
        return fileNameToDo;
    }

    public void setFileNameToDo(String fileNameToDo) {
        this.fileNameToDo = fileNameToDo;
    }

    public String getPathFile() {
        return filePathToDo;
    }

    public void setPathFile(String filePathToDo) {
        this.filePathToDo = filePathToDo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ToDo toDo = (ToDo) o;

        if (id != null ? !id.equals(toDo.id) : toDo.id != null) return false;
        if (dateToDo != null ? !dateToDo.equals(toDo.dateToDo) : toDo.dateToDo != null) return false;
        if (nameToDo != null ? !nameToDo.equals(toDo.nameToDo) : toDo.nameToDo != null) return false;
        if (statusToDo != null ? !statusToDo.equals(toDo.statusToDo) : toDo.statusToDo != null) return false;
        if (fileNameToDo != null ? !fileNameToDo.equals(toDo.fileNameToDo) : toDo.fileNameToDo != null) return false;
        if (filePathToDo != null ? !filePathToDo.equals(toDo.filePathToDo) : toDo.filePathToDo != null) return false;
        if (fileOrigNameToDo != null ? !fileOrigNameToDo.equals(toDo.fileOrigNameToDo) : toDo.fileOrigNameToDo != null) return false;
        return userId != null ? userId.equals(toDo.userId) : toDo.userId == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = result + (dateToDo != null ? dateToDo.hashCode() : 0);
        result = result + (nameToDo != null ? nameToDo.hashCode() : 0);
        result = result + (statusToDo != null ? statusToDo.hashCode() : 0);
        result = result + (fileNameToDo != null ? fileNameToDo.hashCode() : 0);
        result = result + (fileOrigNameToDo != null ? fileOrigNameToDo.hashCode() : 0);
        result = result + (filePathToDo != null ? filePathToDo.hashCode() : 0);
        result = result + (userId != null ? userId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ToDo{" +
                "id=" + id +
                ", dateToDo=" + dateToDo +
                ", nameToDo='" + nameToDo + '\'' +
                ", statusToDo=" + statusToDo +
                ", fileOrigNameToDo='" + fileOrigNameToDo + '\'' +
                ", fileNameToDo='" + fileNameToDo + '\'' +
                ", filePathToDo='" + filePathToDo + '\'' +
                '}';
    }

}
