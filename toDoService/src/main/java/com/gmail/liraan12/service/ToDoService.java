package com.gmail.liraan12.service;

import com.gmail.liraan12.connector.Connect;
import com.gmail.liraan12.dao.ToDoDAO;
import com.gmail.liraan12.entity.ToDo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ToDoService extends Connect implements ToDoDAO {

    Connection connection = getConnection();

    @Override
    public void add(ToDo toDo) throws SQLException {
        PreparedStatement preparedStatement = null;
        Connection connection = getConnection();

        String sql = "INSERT INTO TASKS (USERID, TASKDATE, TASKNAME, TASKSTATUS, TASKFILE, TASKORIGFILE, TASKPATHFILE) VALUES(?, ?, ?, ?, ?, ?, ?)";

        try {
            System.out.println(toDo.getDateToDo() + " " + "TODOSERVICE");
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, toDo.getUserId());
            preparedStatement.setDate(2, toDo.getDateToDo());
            preparedStatement.setString(3, toDo.getNameToDo());
            preparedStatement.setByte(4, toDo.getStatusToDo());
            preparedStatement.setString(5, toDo.getFileNameToDo());
            preparedStatement.setString(6, toDo.getFileOrigNameToDo());
            preparedStatement.setString(7, toDo.getPathFile());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }


    }
    @Override
    public ToDo getById(Integer id) throws SQLException {
        PreparedStatement preparedStatement = null;

        String sql = "SELECT ID, TASKDATE, TASKNAME, TASKSTATUS, TASKFILE, USERID, TASKORIGFILE, TASKPATHFILE FROM TASKS WHERE ID=? ORDER BY TASKDATE";

        Connection connection = getConnection();

        ToDo toDo = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                toDo= new ToDo();
                toDo.setId(resultSet.getInt("ID"));
                toDo.setDateToDo(resultSet.getDate("TASKDATE"));
                toDo.setNameToDo(resultSet.getString("TASKNAME"));
                toDo.setStatusToDo(resultSet.getByte("TASKSTATUS"));
                toDo.setFileNameToDo(resultSet.getString("TASKFILE"));
                toDo.setUserId(resultSet.getInt("USERID"));
                toDo.setFileOrigNameToDo(resultSet.getString("TASKORIGFILE"));
                toDo.setPathFile(resultSet.getString("TASKPATHFILE"));
            }

//            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return toDo;
    }

    @Override
    public List<ToDo> getAllByIdActive(Integer userId) throws SQLException {
        List<ToDo> listToDo = null;

        String sql = "SELECT ID, TASKDATE, TASKNAME, TASKFILE, USERID, TASKORIGFILE, TASKPATHFILE FROM TASKS WHERE USERID=? AND taskstatus=? ORDER BY TASKDATE";

        Connection connection = getConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);;
            preparedStatement.setByte(2,  (byte) 1);;

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                listToDo = new ArrayList<>();
            }

            while (resultSet.next()) {
                ToDo toDo = new ToDo();
                toDo.setId(resultSet.getInt("ID"));
                toDo.setDateToDo(resultSet.getDate("TASKDATE"));
                toDo.setNameToDo(resultSet.getString("TASKNAME"));
                toDo.setStatusToDo((byte) 1);
                toDo.setFileNameToDo(resultSet.getString("TASKFILE"));
                toDo.setUserId(resultSet.getInt("USERID"));
                toDo.setFileOrigNameToDo(resultSet.getString("TASKORIGFILE"));
                toDo.setPathFile(resultSet.getString("TASKPATHFILE"));

                listToDo.add(toDo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return listToDo;
    }

    @Override
    public List<ToDo> getAllByIdActive(Integer userId, Date date) throws SQLException {
        List<ToDo> listToDo = null;

        String sql = "SELECT ID, TASKNAME, TASKFILE, USERID, TASKORIGFILE, TASKPATHFILE FROM TASKS WHERE USERID=? AND taskstatus=? AND TASKDATE=? ORDER BY TASKDATE";

        Connection connection = getConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            preparedStatement.setByte(2,  (byte) 1);
            preparedStatement.setDate(3,  date);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                listToDo = new ArrayList<>();
            }

            while (resultSet.next()) {
                ToDo toDo = new ToDo();
                toDo.setId(resultSet.getInt("ID"));
                toDo.setDateToDo(date);
                toDo.setNameToDo(resultSet.getString("TASKNAME"));
                toDo.setStatusToDo((byte) 1);
                toDo.setFileNameToDo(resultSet.getString("TASKFILE"));
                toDo.setUserId(resultSet.getInt("USERID"));
                toDo.setFileOrigNameToDo(resultSet.getString("TASKORIGFILE"));
                toDo.setPathFile(resultSet.getString("TASKPATHFILE"));

                listToDo.add(toDo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return listToDo;
    }

    @Override
    public List<ToDo> getAllByIdActive(Integer userId, Date today, Date tomorrow) throws SQLException {
        List<ToDo> listToDo = null;

        String sql = "SELECT ID, TASKDATE, TASKNAME, TASKFILE, USERID, TASKORIGFILE, TASKPATHFILE FROM TASKS WHERE USERID=? AND taskstatus=? AND TASKDATE!=? AND TASKDATE!=? ORDER BY TASKDATE";

        Connection connection = getConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);;
            preparedStatement.setByte(2,  (byte) 1);
            preparedStatement.setDate(3,  today);
            preparedStatement.setDate(4,  tomorrow);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                listToDo = new ArrayList<>();
            }

            while (resultSet.next()) {
                ToDo toDo = new ToDo();
                toDo.setId(resultSet.getInt("ID"));
                toDo.setDateToDo(resultSet.getDate("TASKDATE"));
                toDo.setNameToDo(resultSet.getString("TASKNAME"));
                toDo.setStatusToDo((byte) 1);
                toDo.setFileNameToDo(resultSet.getString("TASKFILE"));
                toDo.setUserId(resultSet.getInt("USERID"));
                toDo.setFileOrigNameToDo(resultSet.getString("TASKORIGFILE"));
                toDo.setPathFile(resultSet.getString("TASKPATHFILE"));

                listToDo.add(toDo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return listToDo;
    }

    @Override
    public List<ToDo> getAllByIdDeleted(Integer userId) throws SQLException {
        List<ToDo> listToDo = null;

        String sql = "SELECT ID, TASKDATE, TASKNAME, TASKFILE, USERID, TASKORIGFILE, TASKPATHFILE FROM TASKS WHERE USERID=? AND taskstatus=? ORDER BY TASKDATE";

        Connection connection = getConnection();

        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            preparedStatement.setByte(2, (byte)0);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.isBeforeFirst()) {
                listToDo = new ArrayList<>();
            }

            while (resultSet.next()) {
                ToDo toDo = new ToDo();
                toDo.setId(resultSet.getInt("ID"));
                toDo.setDateToDo(resultSet.getDate("TASKDATE"));
                toDo.setNameToDo(resultSet.getString("TASKNAME"));
                toDo.setStatusToDo((byte)0);
                toDo.setFileNameToDo(resultSet.getString("TASKFILE"));
                toDo.setUserId(resultSet.getInt("USERID"));
                toDo.setFileOrigNameToDo(resultSet.getString("TASKORIGFILE"));
                toDo.setPathFile(resultSet.getString("TASKPATHFILE"));

                listToDo.add(toDo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return listToDo;
    }

    @Override
    public ToDo getFileName(String fileName) throws SQLException {
        PreparedStatement preparedStatement = null;

        System.out.println(fileName);

        String sql = "SELECT ID, TASKDATE, TASKNAME, TASKSTATUS, TASKFILE, USERID, TASKORIGFILE, TASKPATHFILE FROM TASKS WHERE TASKFILE=?  ORDER BY TASKDATE";

        Connection connection = getConnection();

        ToDo toDo = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, fileName);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                toDo.setId(resultSet.getInt("ID"));
                toDo.setDateToDo(resultSet.getDate("TASKDATE"));
                toDo.setNameToDo(resultSet.getString("TASKNAME"));
                toDo.setStatusToDo(resultSet.getByte("TASKSTATUS"));
                toDo.setFileNameToDo(resultSet.getString("TASKFILE"));
                toDo.setUserId(resultSet.getInt("USERID"));
                toDo.setFileOrigNameToDo(resultSet.getString("TASKORIGFILE"));
                toDo.setPathFile(resultSet.getString("TASKPATHFILE"));
            }

//            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return toDo;
    }

    @Override
    public void update(ToDo toDo) throws SQLException {
        PreparedStatement preparedStatement = null;
        Connection connection = getConnection();

        String sql = "UPDATE TASKS SET TASKDATE=?, TASKNAME=?, TASKSTATUS=?, TASKFILE=?, TASKORIGFILE=?, TASKPATHFILE=? WHERE ID=?";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setDate(1, toDo.getDateToDo());
            preparedStatement.setString(2, toDo.getNameToDo());
            preparedStatement.setByte(3, toDo.getStatusToDo());
            preparedStatement.setString(4, toDo.getFileNameToDo());
            preparedStatement.setString(5, toDo.getFileOrigNameToDo());
            preparedStatement.setString(6, toDo.getPathFile());
            preparedStatement.setInt(7, toDo.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void remove(ToDo toDo) throws SQLException {
        PreparedStatement preparedStatement = null;

        Connection connection = getConnection();

        String sql = "DELETE FROM TASKS WHERE ID=?";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, toDo.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}
