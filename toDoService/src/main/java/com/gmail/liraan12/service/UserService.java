package com.gmail.liraan12.service;

import com.gmail.liraan12.connector.Connect;
import com.gmail.liraan12.dao.UserDAO;
import com.gmail.liraan12.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserService extends Connect implements UserDAO {
    Connection connection = getConnection();

    @Override
    public boolean add(User user) throws SQLException {
        boolean b = true;
        PreparedStatement preparedStatement = null;

        String sql = "INSERT INTO USERS (FIRSTNAME, LASTNAME, LOGIN, PASSWORD) VALUES(?, ?, ?, ?)";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            b = false;
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return b;
    }

    @Override
    public List<User> getAll() throws SQLException {
        List<User> listUsers = null;

        String sql = "SELECT USERID, FIRSTNAME, LASTNAME, LOGIN, PASSWORD FROM USERS";

        Statement statement = null;
        try {
            statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.isBeforeFirst()) {
                listUsers = new ArrayList<>();
            }

            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("USERID"));
                user.setFirstName(resultSet.getString("FIRSTNAME"));
                user.setLastName(resultSet.getString("LASTNAME"));
                user.setLogin(resultSet.getString("LOGIN"));
                user.setPassword(resultSet.getString("PASSWORD"));

                listUsers.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return listUsers;
    }

    @Override
    public User getById(Integer id) throws SQLException {
        PreparedStatement preparedStatement = null;

        String sql = "SELECT USERID, FIRSTNAME, LASTNAME, LOGIN, PASSWORD FROM USERS WHERE USERID=?";

        User user = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("USERID"));
                user.setFirstName(resultSet.getString("FIRSTNAME"));
                user.setLastName(resultSet.getString("LASTNAME"));
                user.setLogin(resultSet.getString("LOGIN"));
                user.setPassword(resultSet.getString("PASSWORD"));
            }

//            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return user;
    }

    @Override
    public User getByLoginPassword(String login, String password) throws SQLException {
        PreparedStatement preparedStatement = null;

        String sql = "SELECT USERID, FIRSTNAME, LASTNAME, LOGIN, PASSWORD FROM USERS WHERE LOGIN=? AND PASSWORD=?";

        User user = null;
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("USERID"));
                user.setFirstName(resultSet.getString("FIRSTNAME"));
                user.setLastName(resultSet.getString("LASTNAME"));
                user.setLogin(resultSet.getString("LOGIN"));
                user.setPassword(resultSet.getString("PASSWORD"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return user;
    }

    @Override
    public void update(User user) throws SQLException {
        PreparedStatement preparedStatement = null;

        String sql = "UPDATE USERS SET FIRSTNAME=?, LASTNAME=?, LOGIN=?, PASSWORD=? WHERE USERID=?";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getLogin());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setInt(5, user.getId());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void remove(User user) throws SQLException {
        PreparedStatement preparedStatement = null;

        String sql = "DELETE FROM USERS WHERE USERID=?";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, user.getId());

            int num = preparedStatement.executeUpdate();
            System.out.println(num);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}
