package com.gmail.liraan12.service;

import javax.servlet.http.Part;
import java.io.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;

public class UploadFile {

    public String[] uploadFile(Part file, int userID) throws IOException {
        InputStream inputStream = file.getInputStream();
        //генерируем уникальный путь
        String generatedPath = "C:\\output\\" +userID;
        File directory = new File(generatedPath);
        if (!directory.exists())  directory.mkdirs();
        // генерируем уникальное имя
        String resultFileName = userID + "_" + System.currentTimeMillis() + "_" + "_" + file.getSubmittedFileName();
        File uploadFile = new File(generatedPath, resultFileName);
        FileOutputStream outputStream = new FileOutputStream(uploadFile);
        int read = 0;
        byte[] bytes = new byte[1024];
        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        outputStream.close();
        String[] file_and_path = {resultFileName, generatedPath};

        return file_and_path;
    }

    public boolean deleteFile(String FileName, String pathFile) throws IOException {
        System.out.println(FileName + " " + pathFile);
        File file = new File(pathFile, FileName);
        return file.delete();
    }

}
