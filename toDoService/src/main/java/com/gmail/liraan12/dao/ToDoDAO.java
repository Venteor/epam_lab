package com.gmail.liraan12.dao;

import com.gmail.liraan12.entity.ToDo;

import java.sql.SQLException;
import java.sql.Date;
import java.util.List;

public interface ToDoDAO {

    void add(ToDo toDo) throws SQLException;

    List<ToDo> getAllByIdActive(Integer userId) throws SQLException;
    List<ToDo> getAllByIdActive(Integer userId, Date date) throws SQLException;
    List<ToDo> getAllByIdActive(Integer userId, Date today, Date tomorrow) throws SQLException;

    List<ToDo> getAllByIdDeleted(Integer userId) throws SQLException;

    ToDo getById(Integer id) throws SQLException;

    ToDo getFileName(String fileName) throws SQLException;

    void update(ToDo toDo) throws SQLException;

    void remove( ToDo toDo) throws SQLException;
}
