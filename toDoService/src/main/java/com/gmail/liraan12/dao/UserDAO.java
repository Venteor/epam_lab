package com.gmail.liraan12.dao;

import com.gmail.liraan12.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDAO {

    boolean add(User user) throws SQLException;

    List<User> getAll() throws SQLException;

    User getById(Integer id) throws SQLException;

    User getByLoginPassword(String login, String password) throws SQLException;


    void update(User user) throws SQLException;

    void remove(User address) throws SQLException;

}
