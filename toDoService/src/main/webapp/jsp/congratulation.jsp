<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Congratulation</title>
</head>
<body>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<div class="congrats">
    <h1>Congratulation! <%= request.getParameter("firstname") %>, your registration is done! </h1>
</div>

<p align="center">Please,&nbsp <a href="login.html">sign in</a>&nbsp and start work with TODO list!</p>

</body>
</html>
