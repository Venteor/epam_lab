<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
<link rel="stylesheet" href="css/style.css" type="text/css"/>
<div class="error">
    Error: ${error}
</div>
<p align="center">Please,&nbsp <a href=${previouspage}>try again</a>.</p>
</body>
</html>
