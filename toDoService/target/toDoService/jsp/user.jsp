
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ToDoList</title>
    <link rel="stylesheet" href="css/main.css" type="text/css"/>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
<header>
    <h1>  ${user.login}'s ToDoList</h1>
    <h4><a class="logout" href="logout" >Log out </a></h4>
</header>
<div class="container">
    <div id="center">
        <div class="frame">
            <form name="newTask" method="POST" action="todo" autocomplete="off" enctype="multipart/form-data">
                <h1>Task Manager</h1>
                <input type="hidden" name="userId" value="${user.id}">
                <input type="hidden" name="add" value="add">
                <hr/>
                <br>
                <br>

                <input type="date" id="date" class="form-control"
                       placeholder="yyyy-MM-dd" name="date" value="<%= java.time.LocalDate.now() %>"/>
                <input type="text"  name="task" value="" placeholder="New TODO" required/>
                <input id="file" type="file" enctype="multipart/form-data" name="file"/>
                <br>
                <input id="submit" class="buttonExecute" type="submit" value="Submit"/>
                <input id="clear" class="buttonDelete" type="reset" value="Clear"/>
            </form>
            <hr/>
            <br>
        </div>
        <br>
        <br>
        <div class="container">
            <form class="navigation" method="POST" action="todo" autocomplete="off">
                <p><input type="submit" class="navigate" name="navigation" value="allTasks"/></p>
                <p><input type="submit" class="navigate" name="navigation" value="today"/></p>
                <p><input type="submit" class="navigate" name="navigation" value="tomorrow"/></p>
                <p><input type="submit" class="navigate" name="navigation" value="someday"/></p>
                <p><input type="submit" class="navigate" name="navigation" value="recycleBin"/></p>
            </form>

            <form method="POST" action="todo" autocomplete="off">

                <input type="submit" name="delete" class="buttonDelete" value="delete"/>
                <input type="submit" name="execute" class="buttonExecute" value="execute"/>

                <br>
                <br>
                <table id = "tableTASK">

                    <tr>
                        <th class="thcheck">Check</th>
                        <th class="thdate">Date</th>
                        <th>Note</th>
                        <th class="thfile">File</th>

                    </tr>

                    <c:forEach items="${todolist}" var="list">

                        <tr>
                            <td><input type="checkbox" name="idtask" value="${list.id}"></td>
                            <td><c:out value="${list.dateToDo}"/></td>
                            <td><c:out value="${list.nameToDo}"/></td>
                            <td id="filename">
                                ${list.fileOrigNameToDo}
                                    <button class="deleteFile" type="submit" id="${list.id}" name="Delete File" value="${list.id}">Delete</button>
                                <script>
                                    var filename ='${list.fileOrigNameToDo}'
                                    document.getElementById('${list.id}').style.display = 'none';
                                    if(filename.length > 0){
                                        document.getElementById('${list.id}').style.display = 'inline';
                                    }
                                </script>
                            </td>
                        </tr>

                    </c:forEach>

                </table>
            </form>
        </div>
    </div>
</div>

</body>
</html>
