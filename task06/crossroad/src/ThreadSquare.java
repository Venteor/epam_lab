import crossway.CrossWay;
import settings.Settings;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

import static settings.Settings.*;

/**
 * Created by vcnuv on 07.06.2018.
 */
public class ThreadSquare implements Runnable{
    private final boolean horVert;
    private int lastX, lastY, x, y;
    private int direction = 1;
    JButton button;
    private final Container contentPane;
    private boolean onCross = false;
    private CrossWay crossWay;

    public ThreadSquare(boolean horVert, CrossWay crossWay){
        this.crossWay = crossWay;
        this.horVert = horVert;
        if(horVert){
            x = 0;
            y = Settings.POINT_CROSS_ROAD.y + (int)(Math.random() * (Settings.DIMENSION_CROSS_ROAD.height - Settings.HEIGHT_SQUARE));
        } else {
            x = Settings.POINT_CROSS_ROAD.x + (int)(Math.random() * (Settings.DIMENSION_CROSS_ROAD.width - WIDTH_SQUARE));
            y = 0;
        }
        this.contentPane = Settings.contentPane;
        initButton();
    }

    @Override
    public void run(){
        while(!taskStop){
            move();
            getNextPoint();
            synchronized (crossWay) {
                if(isCrossWay() && !onCross){
                    if(!crossWay.inCrossWay(button)) {
                        x = lastX;
                        y = lastY;
                    }
                }
                if(!isCrossWay() && onCross){
                    crossWay.outCross();
                }
            }
            onCross = isCrossWay();
        }
    }
    private boolean isCrossWay(){
        if(horVert) {
            if ((x > POINT_CROSS_ROAD.x - WIDTH_SQUARE) && (x < POINT_CROSS_ROAD.x + DIMENSION_CROSS_ROAD.width))
                return true;
        } else {
            if((y > POINT_CROSS_ROAD.y - HEIGHT_SQUARE) && (y < POINT_CROSS_ROAD.x + DIMENSION_CROSS_ROAD.height))
                return true;
        }
        return false;
        }


    private void getNextPoint(){
        lastX = x;
        lastY = y;
        if(horVert){
            x += Settings.STEP_SQUARE * direction;
            if(x >= Settings.FRAME_WIDTH - WIDTH_SQUARE-10){
                direction = -1;
            }
            if(x<=0){
                direction = 1;
            }
        } else {
            y += Settings.STEP_SQUARE * direction;
            if(y>=Settings.FRAME_HEIGHT - Settings.HEIGHT_SQUARE - 30){
                direction = -1;
            }
            if(y<=0){
                direction = 1;
            }
        }
    }
    private void move(){
        button.setBounds(x,y, WIDTH_SQUARE,Settings.HEIGHT_SQUARE);
        try{
            Thread.sleep(Settings.DELAY_SQUARE);
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    private void initButton(){
        Random random = new Random();
        Color[] color = {new Color(0,0,0), new Color(255,0,0), new Color(0,255,0),new Color(255,165,122),
                new Color(255,215,0),new Color(0,0,255),new Color(255,0,255),new Color(190,190,190)};
        button = new JButton("" + (++Settings.counter));
        button.setLocation(x,y);
        button.setSize(WIDTH_SQUARE, Settings.HEIGHT_SQUARE);
        button.setMargin(new Insets(0,0,0,0));
        button.setForeground(Settings.COLOR_TEXT_SQUARE);
        button.setBackground(color[random.nextInt(color.length)]);
        contentPane.add(button);
    }
}
