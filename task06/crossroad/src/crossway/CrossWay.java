package crossway;

import settings.Settings;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class CrossWay {
    private List<Integer> colorBox = new ArrayList<>();

    /**
     * Entering on CrossWay
     */
    public boolean inCrossWay(JButton button){
        if ((colorBox.size() > 0) && ((Settings.numberTask) || (!colorBox.contains((button.getBackground().getRGB()))))){
            try{
                this.wait();
                return false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        colorBox.add(button.getBackground().getRGB());
        return true;
    }
    public void outCross() {
        colorBox.remove(0);
        this.notifyAll();
    }
}
