import crossway.CrossWay;
import settings.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static settings.Settings.*;


public class CrossRoad {
    private JFrame frame;
    private JButton addHorSquareButton, addVertSquareButton, changeTaskButton, exitButton;
    private MyDrawPanel drawPanel;
    List<Thread> listThread = new ArrayList<>();
    private CrossWay crossWay = new CrossWay();




    public static class MyDrawPanel extends JPanel {
        public void paintComponent(Graphics g){
            g.setColor(Color.gray);
            g.fillRect(0,0, Settings.FRAME_WIDTH,Settings.FRAME_HEIGHT);

            g.setColor(Settings.COLOR_OF_LINE);
            g.drawLine(Settings.POINT_CROSS_ROAD.x,0,Settings.POINT_CROSS_ROAD.x,Settings.FRAME_HEIGHT);

            g.setColor(Settings.COLOR_OF_LINE);
            g.drawLine(Settings.POINT_CROSS_ROAD.x+Settings.DIMENSION_CROSS_ROAD.width,0,
                    Settings.POINT_CROSS_ROAD.x+Settings.DIMENSION_CROSS_ROAD.width,Settings.FRAME_HEIGHT);

            g.setColor(Settings.COLOR_OF_LINE);
            g.drawLine(0,Settings.POINT_CROSS_ROAD.y,Settings.FRAME_WIDTH,Settings.POINT_CROSS_ROAD.y);

            g.setColor(Settings.COLOR_OF_LINE);
            g.drawLine(0,Settings.POINT_CROSS_ROAD.y+Settings.DIMENSION_CROSS_ROAD.height,
                    Settings.FRAME_WIDTH,Settings.POINT_CROSS_ROAD.y+Settings.DIMENSION_CROSS_ROAD.height);
        }
    }


    public void go(){


        addHorSquareButton = new JButton("<html><font size=5><p align = center>Добавить горизонтальный квадрат");
        addHorSquareButton.setLocation(0,0);
        addHorSquareButton.setSize(240,240);
        addHorSquareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listThread.add(new Thread(new ThreadSquare(true, crossWay)));
                listThread.get(listThread.size()-1).start();
            }
        });

        addVertSquareButton = new JButton("<html><font size=5><p align = center>Добавить вертикальный квадрат");
        addVertSquareButton.setLocation(341,0);
        addVertSquareButton.setSize(240,240);
        addVertSquareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listThread.add(new Thread(new ThreadSquare(false, crossWay)));
                listThread.get(listThread.size()-1).start();
            }
        });

        changeTaskButton = new JButton(Settings.BUTTON_TEXT_PART2);
        changeTaskButton.setLocation(0,341);
        changeTaskButton.setSize(240,220);
        changeTaskButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(Settings.numberTask) {
                    changeTaskButton.setText(Settings.BUTTON_TEXT_PART1);
                    frame.setTitle(Settings.FRAME_TEXT_TASK2);
                    Settings.numberTask = false;
                }else{
                    changeTaskButton.setText(Settings.BUTTON_TEXT_PART2);
                    frame.setTitle(Settings.FRAME_TEXT_TASK1);
                    Settings.numberTask = true;
                }
            }
        });

        exitButton = new JButton("<html><font size=5>Выход");
        exitButton.setLocation(341,341);
        exitButton.setSize(240,220);
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taskStop = true;
                synchronized (crossWay){
                    crossWay.notifyAll();
                }
                try {
                    for(Thread thread : listThread) thread.join();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                System.exit(0);
            }
        });

        initDrawPanel();
        initFrame();
    }

    private void initDrawPanel() {
        drawPanel = new MyDrawPanel();
        drawPanel.setBackground(Color.white);
        drawPanel.setLayout(null);
        drawPanel.add(addHorSquareButton);
        drawPanel.add(addVertSquareButton);
        drawPanel.add(changeTaskButton);
        drawPanel.add(exitButton);
        Settings.contentPane = drawPanel;
    }

    private void initFrame() {
        frame = new JFrame(Settings.FRAME_TEXT_TASK1);
        frame.getContentPane().add(drawPanel, BorderLayout.CENTER);
        frame.setSize(Settings.FRAME_WIDTH, Settings.FRAME_HEIGHT);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setIconImage(new ImageIcon(Settings.ICON_FILE_NAME).getImage());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        CrossRoad cr = new CrossRoad();
        cr.go();
    }
}

