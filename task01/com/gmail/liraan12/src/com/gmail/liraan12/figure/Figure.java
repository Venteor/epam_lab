package com.gmail.liraan12.figure;

/**
 * Created by Trukhan AD on 16.04.2019
 * Abstract Class. Create methods getArea, getPerimeter, getMinSize.
 * Override methods toString.
 */
public abstract class Figure {
    public abstract double getArea();
    public abstract double getPerimeter();
    //public abstract double getMinSize();
    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }

}
