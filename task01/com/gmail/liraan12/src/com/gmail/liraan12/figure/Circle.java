package com.gmail.liraan12.figure;

public abstract class Circle extends Figure {
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
    return radius;
    }
    public void setRadius (double radius) {
        this.radius = radius;
    }
    @Override
    public double getArea() {
        return Math.PI*radius*radius;
    }
    public double getPerimeter() {
        return Math.PI*2*radius;
    }
    @Override
    public String toString() {
        return super.toString() + ": radius = " + radius;
    }
}
