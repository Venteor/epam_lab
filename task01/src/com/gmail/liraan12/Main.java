/**
 * @author Trukhan AD on 01.05.2019
 */

package com.gmail.liraan12;

import java.lang.System;
import com.gmail.liraan12.figure.*;
import com.gmail.liraan12.box.*;
import com.gmail.liraan12.film.*;
import com.gmail.liraan12.paper.*;

/**
 * Class for test Box
 */


public class Main {
    public static void main(String args[]){
        Box box = new Box(10);
        PaperCircle paperCircle1 = new PaperCircle(1);
        FilmCircle filmCircle2 = new FilmCircle(20);
        PaperCircle paperCircle3 = new PaperCircle(3);
        PaperCircle paperCircle4 = new PaperCircle(30);
        paperCircle1.setColor(Paper.Color.BlUE);
        PaperCircle paperCircle5 = new PaperCircle(paperCircle3);
        FilmCircle filmCircle6 = new FilmCircle(filmCircle2);
        paperCircle5.setColor(Paper.Color.RED);
        paperCircle5.setColor(Paper.Color.BLACK);
        /*
        System.out.println(paperCircle1.toString() + "; perimeter: " + paperCircle1.getPerimeter() + "; area: " + paperCircle1.getArea());
        System.out.println(paperCircle4.toString() + "; perimeter: " + paperCircle4.getPerimeter() + "; area: " + paperCircle4.getArea());
        System.out.println(paperCircle5.toString() + "; perimeter: " + paperCircle5.getPerimeter() + "; area: " + paperCircle5.getArea());
        System.out.println(filmCircle6.toString() + "; perimeter: " + filmCircle6.getPerimeter() + "; area: " + filmCircle6.getArea());
        */

        FilmRectangle filmRectangle1 = new FilmRectangle(1,2);
        FilmRectangle filmRectangle2 = new FilmRectangle(2,3);
        PaperRectangle paperRectangle3 = new PaperRectangle(3,4);
        PaperRectangle paperRectangle4 = new PaperRectangle(paperRectangle3);
        paperRectangle4.setColor(Paper.Color.GREEN);
        paperRectangle4.setColor(Paper.Color.BLACK);
        /*
        System.out.println(filmRectangle1.toString() + "; perimeter: " + filmRectangle1.getPerimeter() + "; area: " + filmRectangle1.getArea());
        System.out.println(filmRectangle2.toString() + "; perimeter: " + filmRectangle2.getPerimeter() + "; area: " + filmRectangle2.getArea());
        System.out.println(paperRectangle4.toString() + "; perimeter: " + paperRectangle4.getPerimeter() + "; area: " + paperRectangle4.getArea());
        */

        PaperTriangle paperTriangle1 = new PaperTriangle(20);
        PaperTriangle paperTriangle2 = new PaperTriangle(30);
        PaperTriangle paperTriangle3 = new PaperTriangle(40);
        FilmTriangle filmTriangle5 = new FilmTriangle(4);
        paperTriangle1.setColor(Paper.Color.WHITE);
        paperTriangle2.setColor(Paper.Color.BLACK);
        PaperTriangle paperTriangle4 = new PaperTriangle(paperTriangle3);
        paperTriangle4.setColor(Paper.Color.GREEN);
        paperTriangle4.setColor(Paper.Color.BLACK);
        /*
        System.out.println(paperTriangle1.toString() + "; perimeter: " + paperTriangle1.getPerimeter() + "; area: " + paperTriangle1.getArea());
        System.out.println(paperTriangle2.toString() + "; perimeter: " + paperTriangle2.getPerimeter() + "; area: " + paperTriangle2.getArea());
        System.out.println(paperTriangle4.toString() + "; perimeter: " + paperTriangle4.getPerimeter() + "; area: " + paperTriangle4.getArea());
        */

        box.addFigure(paperCircle1);
        box.addFigure(paperCircle5);
        box.addFigure(paperCircle1);
        box.addFigure(5,filmCircle6);
        box.addFigure(9,paperCircle4);
        box.addFigure(filmRectangle1);
        box.addFigure(filmRectangle2);
        box.addFigure(paperRectangle4);
        box.addFigure(paperTriangle1);
        box.addFigure(paperTriangle2);
        box.addFigure(paperTriangle4);
        box.setFigure(6, filmCircle6);
        box.setFigure(7, filmTriangle5);
        box.removeFigure(0);
        for (int i = 0; i < 10; i++){
            System.out.println(box.getFigure(i));
        }
        System.out.println(box.getFigure(3));
        System.out.println("Count of figures in Box: " + box.getCount());
        System.out.println("Area of all figures in Box: " + box.getArea());
        System.out.println("Perimeter of all figures in Box: " + box.getPerimeter());
        System.out.println("Figure index 5: " + box.getFigure(5));
        System.out.println("The Box has red circle radius 1.5 - " + box.findByFigure(paperCircle5));
        System.out.println("The Box has blue circle radius 1 - " + box.findByFigure(paperCircle1));
        Figure[] boxCircle = box.getAllCircles();
        for (int i = 0; i < boxCircle.length; i++){
            System.out.println(boxCircle[i]);
        }
        Figure[] boxFilm = box.getAllFilmFigure();
        for (int i = 0; i < boxFilm.length; i++){
            System.out.println(boxFilm[i]);
        }
    }
}
