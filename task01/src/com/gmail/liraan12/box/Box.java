/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.box;

import com.gmail.liraan12.figure.Circle;
import com.gmail.liraan12.figure.Figure;
import com.gmail.liraan12.film.Film;

/**
 * Provides provides storage and operations figures.
 */

public class Box {
    private final int SIZE;
    private Figure[] figures;

    public Box(int size) {
        this.SIZE = size;
        figures = new Figure[this.SIZE];
    }

    /**
     * Adds figure in Box.
     * Checks adding same figure.
     * Finds vacant place in massive and adds newFigure.
     * @param newFigure
     */

    public void addFigure(Figure newFigure) {
        boolean sameFigure = false;
        for (int i = 0; i < figures.length; i++){
            if (figures[i] == newFigure) {
             sameFigure = true;
            }
        }
        for (int i = 0; i < figures.length; i++){
            if (figures[i] == null && !sameFigure) {
                figures[i] = newFigure;
                break;
            }
        }
    }

    /**
     * Adds figure in Box by index.
     * Checks adding same figure.
     * Adds newFigure by index.
     * @param newFigure
     * @param index
     */

    public void addFigure(int index, Figure newFigure) {
        boolean sameFigure = false;
        for (int i = 0; i < figures.length; i++){
            if (figures[i] == newFigure) {
                sameFigure = true;
                break;
            }
        }
        if (figures[index] == null && !sameFigure && index >= 0 && index < SIZE) {
            figures[index] = newFigure;
            }
    }

    /**
     * Checks all cells of box and summarizes figures in Box.
     * @return count of figures in Box.
     */

    public int getCount() {
        int count = 0;
        for (int i = 0; i < SIZE; i++) {
            if (figures[i] != null) {
                count++;
            }
        }
        return count;
    }

    /**
     * Checks all cells of box and summarizes perimeter of figures in Box.
     * @return perimeter of all figures in Box.
     */

    public double getPerimeter() {
        double perimeter = 0;
        for (int i = 0; i < SIZE; i++) {
            if (figures[i] != null){
            perimeter += figures[i].getPerimeter();
            }
        }
        return perimeter;
    }

    /**
     * Checks all cells of box and summarizes area of figures in Box.
     * @return area of all figures in Box.
     */

    public double getArea() {
        double area = 0;
        for (int i = 0; i < SIZE; i++) {
            if(figures[i] != null){
            area += figures[i].getArea();
            }
        }
        return area;
    }

    /**
     * @param index
     * @return null if index out of range Box
     * @return figures[index]
     */

    public Figure getFigure(int index) {
        if (index >= SIZE || index < 0) {
           return null;
       }
       else {
           return figures[index];
       }
    }

    /**
     * Replaces figures[index] by figure and return old figures[index].
     * @param index
     * @param figure
     * @return null if index out of range Box
     * @return oldFigure
     */

    public Figure setFigure(int index, Figure figure) {
        for (int i = 0; i < figures.length; i++) {
            if (figures[i] == figure) {
                return null;
            }
        }
        if (index >= SIZE || index < 0){
            return null;
        }
        Figure oldFigure = figures[index];
        figures[index] = figure;
        return oldFigure;
    }

    /**
     * Removes figures[index] and return removed figures[index].
     * @param index
     * @return figure
     */

    public Figure removeFigure(int index) {
        Figure figure = figures[index];
        figures[index] = null;
        return figure;
    }

    /**
     * Finds equal figure in figures[].
     * @param figure
     * @return true if equal figure found and falsr if not
     */

    public boolean findByFigure(Figure figure){
        if (figure == null) {
            return false;
        }
        for (Figure i: figures){
            if(figure.equals(i)){
                return true;
            }
        }
        return false;
    }

    /**
     * Getting all circle figures.
     * Calculates size of Circle massive.
     * Adds all Circles from Box in massive circles.
     * @return massive of circle figures
     */

    public Figure[] getAllCircles() {
        int size = 0;
        for (Figure figure : figures) {
            if (figure instanceof Circle) {
                size++;
            }
        }
        if (size == 0) {
            return null;
        }

        Figure boxCircles[] = new Figure[size];
        int i=0;
        for (int q = 0; q < figures.length; q++) {
            if (figures[q] instanceof Circle) {
                boxCircles[i++] = figures[q];
            }
        }
        return boxCircles;
    }

    /**
     * Getting all film figures.
     * Calculates size of Film massive.
     * Adds all Film figures from Box in massive film.
     * @return massive of film figures
     */

    public Figure[] getAllFilmFigure() {
        int size = 0;
        for (Figure figure : figures) {
            if (figure instanceof Film) {
                size++;
            }
        }
        if (size == 0) {
            return null;
        }

        Figure boxFilm[] = new Figure[size];
        int i=0;
        for (int q = 0; q < figures.length; q++) {
            if (figures[q] instanceof Film) {
                boxFilm[i++] = figures[q];
            }
        }
        return boxFilm;
    }

}
