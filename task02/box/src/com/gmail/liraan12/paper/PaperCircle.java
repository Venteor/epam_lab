/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.paper;

import com.gmail.liraan12.figure.Circle;
import com.gmail.liraan12.figure.Figure;

import java.util.Objects;
import java.util.*;

/**
 * Provides creating PaperCircle objects.
 */

public class PaperCircle extends Circle implements Paper {
    private Painted painted = new Painted();
    public PaperCircle (double radius) {
        super(radius);
    }
    public PaperCircle (Paper paperFigure){
        super((Figure)paperFigure);
        painted = paperFigure.getPainted();
    }

    /**
     * @return color of object.
     */

    @Override
    public Color getColor(){
        return painted.getColor();
    }

    /**
     * Paints object in color.
     * @param color
     */

    @Override
    public void setColor (Color color){
        painted.setColor(color);
    }

    /**
     * @return painted object
     */

    @Override
    public Painted getPainted(){
        return painted;
    }

    /**
     * @return string consist of name class, it's radius and color.
     */

    @Override
    public String toString(){
        return super.toString() + ", painted = " + painted.getColor();
    }

    /**
     * Equality check.
     * @param o
     * @return true if o equals figure and false if not.
     */

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!super.equals(o)) return false;
        PaperCircle paperCircle = (PaperCircle) o;
        return Objects.equals(paperCircle.painted, painted);
    }

    /**
     * @return hashcode of Circle by painted.
     */

    @Override
    public int hashCode(){
        return Objects.hash(painted);
    }

}
