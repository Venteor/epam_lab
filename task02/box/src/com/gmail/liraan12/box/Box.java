/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.box;


import com.gmail.liraan12.figure.Figure;
import com.gmail.liraan12.paper.Paper;
import com.gmail.liraan12.paper.PaperCircle;

import java.util.*;

/**
 * Provides storage and operations figures.
 */

public class Box {
    private final List <Figure> boxFigures = new ArrayList<>();

    /**
     * Adds a figure in the box.
     * @param figure is any figure for add to the box.
     * @return true if the figure was add.
     */

    public boolean addFigure(Figure figure) {
       if(figure != null && !boxFigures.contains(figure)){
           boxFigures.add(figure);
           return true;
       }
       return false;
    }

    /**
     * Adds a figure in the box.
     * @param figure is any figure for add to the box.
     * @param index of place in box.
     * @return true if the figure was add.
     */

    public boolean addFigure(int index, Figure figure) {
        if(figure != null && !boxFigures.contains(figure) && index >=0 && index < boxFigures.size()){
            boxFigures.add(index, figure);
            return true;
        }
        return false;
    }

    /**
     * Adds a figure in the box.
     * @param figure is list of figures for add to the box.
     * @param index of place in box.
     * @return true if the figures was add.
     */

    public boolean addAllFigure(int index, List<Figure> figure) {
        if(figure != null && !boxFigures.contains(figure) && index >=0 && index < boxFigures.size()){
            boxFigures.addAll(index, figure);
            return true;
        }
        return false;
    }

    /**
     * Adds a figure in the box.
     * @param figure is list of figures for add to the box.
     * @return true if the figures was add.
     */

    public boolean addAllFigure(List<Figure> figure) {
        if(figure != null && !boxFigures.contains(figure)){
            boxFigures.addAll(figure);
            return true;
        }
        return false;
    }

     /**
     * @return size of the box.
     */

    public int getCount() {
        if(boxFigures.size()>0) {
            return boxFigures.size();
        }
        return 0;
    }

    /**
     * Checks all cells of the box and summarizes perimeter of figures in the box.
     * @return perimeter of all figures in Box.
     */

    public double getPerimeter() {
        double perimeter = 0;
        for (int i = 0; i < boxFigures.size(); i++) {
            perimeter += boxFigures.get(i).getPerimeter();
        }
        return perimeter;
    }

    /**
     * Checks all cells of box and summarizes area of figures in Box.
     * @return area of all figures in Box.
     */

    public double getArea() {
        double area = 0;
        for (int i = 0; i < boxFigures.size(); i++) {
            area += boxFigures.get(i).getArea();
        }
        return area;
    }

    /**
     * @param index
     * @return null if index out of the box range.
     * @return boxFigures(index)
     */

    public Figure getFigure(int index) {
        if (index >= boxFigures.size() || index < 0) {
           return null;
       }
       else {
           return boxFigures.get(index);
       }
    }


    /**
     * @param first index of first figure including.
     * @param last index of figure in sublist.
     * @return null if index out of the box range.
     * @return sublist of figures.
     */

    public List<Figure> getAllFigureByIndex(int first, int last) {
        if (first >= boxFigures.size() || first < 0 || last >= boxFigures.size() || last < 0) {
            return null;
        }
        else {
            return boxFigures.subList(first, ++last);
        }
    }


    /**
     * Replaces figures[index] by figure and return old figures[index].
     * @param index
     * @param figure
     * @return null if index out of the box range.
     * @return oldFigure
     */

    public Figure setFigure(int index, Figure figure) {
        if (index >= boxFigures.size() || index < 0){
            return null;
        }
        Figure oldFigure = boxFigures.get(index);
        boxFigures.set(index, figure);
        return oldFigure;
    }

    /**
     * Removes figures[index] and return removed figures[index].
     * @param index
     * @return null if index out of the box range.
     * @return figure
     */

    public Figure removeFigure(int index) {
        if(index >= boxFigures.size() && index < 0) return null;
        Figure figure = boxFigures.get(index);
        boxFigures.remove(index);
        return figure;
    }

    /**
     * Finds equal figure in figures[].
     * @param figure
     * @return true if equal figure found and falsr if not
     */

    public boolean findByFigure(Figure figure){
        if (figure == null) {
            return false;
        }
        if(boxFigures.contains(figure)){
            return true;
        }
        return false;
    }

    /**
     * @param c type of figure
     * Adds all figure's type from Box in new box.
     * @return newBox ArrayList of selected figures
     */

    public ArrayList<Figure> getFigure(Class c) {
        ArrayList<Figure> newBox = new ArrayList<>();
        for (int i = 0; i < boxFigures.size(); i++){
            if(c.isInstance(boxFigures.get(i))){
                newBox.add(boxFigures.get(i));
                    }
                }
        return newBox;
    }

    /**
     * Getting all red circles.
     * Adds all red circles from Box in newBox.
     * @return newBox ArrayList of Film figures
     */

    public ArrayList<Figure> getRedCircle() {
        ArrayList<Figure> newBox = new ArrayList<>();
        Iterator<Figure> iter = boxFigures.iterator();
        while (iter.hasNext()){
            Figure figure = iter.next();
            if(figure instanceof PaperCircle && ((PaperCircle) figure).getColor() == Paper.Color.RED){
                newBox.add(figure);
            }
        }
        return newBox;
    }
    /**
     * Sort figures in the Box by shape.
     * @return boxFigures List of sorted figures
     */
    public List<Figure> sortByShape() {
        Collections.sort(boxFigures, new ComparatorFigure());
        return boxFigures;
    }
    /**
     * Sort figures in the Box by color.
     * @return boxFigures List of sorted figures
     */
    public List<Figure> sortByColor() {
        Collections.sort(boxFigures, new ComparatorFigure.InnerComparator());
        return boxFigures;
    }
    /**
     * Sort figures in the Box by area.
     * @return sorted box
     */
    public void sortByArea() {
        boxFigures.sort(new Comparator<Figure>() {
            @Override
            public int compare(Figure o1, Figure o2) {
                return (int)(Math.round(o1.getArea())-Math.round(o2.getArea()));
            }
        });
    }
    @Override
    public String toString(){
        return "Box { " + boxFigures + "}";
    }
}
