/**
 * @author Trukhan AD on 16.04.2019
 */

package com.gmail.liraan12.film;

import com.gmail.liraan12.figure.Circle;
import com.gmail.liraan12.figure.Figure;

/**
 * Provides creating FilmCircle objects.
 */

public class FilmCircle extends Circle implements Film{
    public FilmCircle (double radius){
        super(radius);
    }
    public FilmCircle(Film filmFigure){
        super((Figure)filmFigure);
    }
}
