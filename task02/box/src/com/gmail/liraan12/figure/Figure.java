/**
 * @author Trukhan AD on 16.04.2019
 * Abstract Class. Super Class for Circle, Triangle, Rectangle
 * Create methods getArea, getPerimeter, getMinSize.
 * Override method toString.
 */

package com.gmail.liraan12.figure;

/**
 * Provides abstraction for Circle, Triangle, Rectangle.
 */

public abstract class Figure {
    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract double getMinSize();
    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }
    @Override
    public boolean equals(Object o){
        return getClass()==o.getClass();
    }

}
