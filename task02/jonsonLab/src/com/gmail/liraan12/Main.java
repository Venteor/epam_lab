package com.gmail.liraan12;

import com.gmail.liraan12.detail.Detail;
import com.gmail.liraan12.workPlace.WorkPlace;

public class Main {
    public static void main(String args[]){
        WorkPlace workPlace = new WorkPlace();
        Detail detail1 = new Detail(1, 1,5);
        Detail detail2 = new Detail(2, 6,4);
        Detail detail3 = new Detail(3, 2,3);
        Detail detail4 = new Detail(4, 6,3);
        Detail detail5 = new Detail(5, 9,1);
        Detail detail6 = new Detail(6, 4,5);
        Detail detail7 = new Detail(7, 2,2);
        Detail detail8 = new Detail(8, 7,7);
        workPlace.addDetail(detail1);
        workPlace.addDetail(detail2);
        workPlace.addDetail(detail3);
        workPlace.addDetail(detail4);
        workPlace.addDetail(detail5);
        workPlace.addDetail(detail6);
        workPlace.addDetail(detail7);
        workPlace.addDetail(detail8);
        System.out.println(workPlace.toString());
        workPlace.sortWorkPlace();
        System.out.println(workPlace.toString());
        WorkPlace workPlace1 = new WorkPlace();
        Detail detail11 = new Detail(1, 12,2);
        Detail detail12 = new Detail(2, 7,9);
        Detail detail13 = new Detail(3, 3,4);
        Detail detail14 = new Detail(4, 5,5);
        Detail detail15 = new Detail(5, 1,3);
        Detail detail16 = new Detail(6, 7,5);
        Detail detail17 = new Detail(7, 2,8);
        workPlace1.addDetail(detail11);
        workPlace1.addDetail(detail12);
        workPlace1.addDetail(detail13);
        workPlace1.addDetail(detail14);
        workPlace1.addDetail(detail15);
        workPlace1.addDetail(detail16);
        workPlace1.addDetail(detail17);
        System.out.println(workPlace1.toString());
        workPlace1.sortWorkPlace();
        System.out.println(workPlace1.toString());
        WorkPlace workPlace2 = new WorkPlace();
        Detail detail21 = new Detail(1, 6,2);
        Detail detail22 = new Detail(2, 3,4);
        Detail detail23 = new Detail(3, 2,2);
        Detail detail24 = new Detail(4, 3,2);
        workPlace2.addDetail(detail21);
        workPlace2.addDetail(detail22);
        workPlace2.addDetail(detail23);
        workPlace2.addDetail(detail24);
        System.out.println(workPlace2.toString());
        workPlace2.sortWorkPlace();
        System.out.println(workPlace2.toString());
    }

}
