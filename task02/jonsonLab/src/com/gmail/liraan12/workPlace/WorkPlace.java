package com.gmail.liraan12.workPlace;

import com.gmail.liraan12.detail.Detail;

import java.util.*;

public class WorkPlace {
    private List <Detail> workPlace = new ArrayList<>();
    /**
     * Adds a detail in the workPace.
     * @param detail is any detail for add to the workspace.
     * @return true if the detail was add.
     */

    public boolean addDetail(Detail detail) {
        if(detail != null){
            workPlace.add(detail);
            return true;
        }
        return false;
    }
    private Detail getDetailMinTime1(List <Detail> details){
        return Collections.min(details, new Comparator<Detail>() {
            @Override
            public int compare(Detail o1, Detail o2) {
                return (int)(o1.getTime1() - o2.getTime1());
            }
        });
    }

    private Detail getDetailMinTime2(List <Detail> details){
        return Collections.min(details, new Comparator<Detail>() {
            @Override
            public int compare(Detail o1, Detail o2) {
                return (int)(o1.getTime2() - o2.getTime2());
            }
        });
    }

    public List <Detail> sortWorkPlace() {
        List <Detail> sortWorkPlace = new ArrayList<>(this.workPlace);
        int j = sortWorkPlace.size()-1, i = 0;
        Iterator listIterator = workPlace.listIterator();
        while (listIterator.hasNext()) {
            Detail o1 = getDetailMinTime1(workPlace);
            Detail o2 = getDetailMinTime2(workPlace);
            if (o1.getTime1() <= o2.getTime2()){
                sortWorkPlace.set(i++, o1);
                workPlace.remove(o1);
            }
            else{
                sortWorkPlace.set(j--, o2);
                workPlace.remove(o2);
            }
        }
        return workPlace = sortWorkPlace;
    }

    public int sumTimes(List<Detail> details) {
        int sum = 0;
        for (Detail o : details){

            sum += o.getTime1();
        }
        return (int)(sum + details.get(details.size()-1).getTime2());
    }

    @Override
    public String toString() {
        return "Work Place:\n" + workPlace + "\n" + "Total time: " + sumTimes(workPlace);
    }
}
