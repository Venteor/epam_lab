package com.gmail.liraan12.detail;

public class Detail {
    private int index, time1, time2;
    public Detail (int index, int time1, int time2){
        this.index = index;
        this.time1 = time1;
        this.time2 = time2;
    }
    public double getIndex() {
        return index;
    }
    public double getTime1() {
        return time1;
    }
    public double getTime2() {
        return time2;
    }
    @Override
    public String toString(){
        return index + " (" + time1 + ", " + time2 + ")";
    }
}
