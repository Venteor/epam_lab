package com.gmail.liraan12;

import com.gmail.liraan12.textChanger.Text;

public class Main {
    public static void main(String args[]){
        String text = "Я из числа тех самых славных малых, которые могут достать все.\n" +
                "Абсолютно все, хоть черта из преисподней.\n" +
                "Такие ребята водятся в любой федеральной тюрьме Америки.\n" +
                "Искупил ли я свою вину, спросите вы?\n" +
                "Реабилитировал ли себя?\n" +
                "Я не вполне знаю, что означают эти слова и какое искупление может быть в тюрьме или колонии.\n" +
                "Парень назвал свои произведения «Три возраста Иисуса», и теперь они украшают гостиную губернатора штата.\n" +
                "Обратите внимание, возвышал голос прокурор, четыре и четыре!\n" +
                "Не шесть выстрелов, а восемь!";
        Text tx = new Text();
        tx.parseText(text);
        System.out.println(tx.printText());
        String str = tx.findNoMatchWords();
        System.out.println("Unique words: " + str);
        tx.changeText('и','х');
        System.out.println(tx.printText());


    }
}
