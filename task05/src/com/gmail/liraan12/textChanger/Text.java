package com.gmail.liraan12.textChanger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    private static final String TEXT_REGEX = "([^!?.]+[!?.]+)";
    private List<Sentence> sentences = new ArrayList<>();
    public void parseText(String textStr){
        Pattern p1 = Pattern.compile(TEXT_REGEX);
        Matcher m1 = p1.matcher(textStr);
        String ss;
        while (m1.find()){
            ss = m1.group().trim();
            if(ss.length() > 0) {
                Sentence sentence = new Sentence();
                sentence.parseSentence(ss);
                sentences.add(sentence);
            }
        }
    }
    public String findNoMatchWords(){
        Sentence s1 = sentences.get(0);
        StringBuilder result = new StringBuilder("");
        for(int j = 0; j < s1.getSize(); j++) {
            Element e1 = s1.getElement(j);
            if(e1 instanceof Word) {
                boolean noMatch = false;
                for (int i = 1; i < sentences.size(); i++) {
                    Sentence s2 = sentences.get(i);
                    noMatch = s2.findWord(e1);
                    if(!noMatch) break;
                }
                if(noMatch) result.append(e1.toString()).append(" ");
            }
        }
        return result.toString();
    }
    public void changeText(char firstChar, char lastChar){
        for(int i = 0; i < sentences.size(); i++){
            Sentence s = sentences.get(i);
            switch (s.lastChar()) {
                case '.':
                    sentences.set(i,s.removeSubstring(firstChar, lastChar));
                    break;
                case '!':
                    s.reverseWords();
                    break;
                case '?':
                    s.removeFirstCharDuplicatesSentence();
                    break;
            }
        }
    }
    public String printText() {
        StringBuilder t = new StringBuilder();
        for(Sentence s : sentences){
            t.append(s.toString()+" ");
        }
        return t.toString();
    }
}
