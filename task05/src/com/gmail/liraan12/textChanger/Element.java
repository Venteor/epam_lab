package com.gmail.liraan12.textChanger;

public abstract class Element {
    String element;
    public Element(String element){
        this.element = element;
    }
    public Element(){
    }

    @Override
    public String toString(){
        return element;
    }

}
