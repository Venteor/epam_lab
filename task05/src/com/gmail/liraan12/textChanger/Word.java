package com.gmail.liraan12.textChanger;


public class Word extends Element {
    String word;
    public Word(String word){
        super(word);
    }
    public Word(){
    }
    //word knows it's size
    public int getLength() {
        return word.length();
    }
    //remove from the word all occurrences of the first character
    public String removeFirstCharDuplicates(){
        String firstChar = toString().toLowerCase().substring(0,1);
        String result = toString().replaceAll("\\B"+firstChar, "");
        return result;
    }

    @Override
    public String toString(){
        return super.toString();
    }
}
