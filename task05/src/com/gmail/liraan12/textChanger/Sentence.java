package com.gmail.liraan12.textChanger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
    private static final String TEXT_REGEX = "([^!\\.\\?\\,\\:\\;\\s]+)|([!\\.\\?\\,\\:\\;\\s])";
    private static final String WORD_REGEX = "[!.?,:; ]";
    private List<Element> elements = new ArrayList<>();

    public void parseSentence(String wordStr){
        Pattern p2 = Pattern.compile(TEXT_REGEX);
        Matcher m2 = p2.matcher(wordStr);
        String ss;
        while (m2.find()){
            ss = m2.group();
            if(ss.length() > 0){
                if(!ss.matches(WORD_REGEX)){
                    Word word = new Word(ss);
                    elements.add(word);
                }
                else {
                    Punctuation punctuation = new Punctuation(ss);
                    elements.add(punctuation);
                }

            }

        }

    }
    public char lastChar(){
        return elements.get(elements.size()-1).toString().charAt(0);
    }
    public void removeFirstCharDuplicatesSentence(){
        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i) instanceof Word){
                Word w = (Word) elements.get(i);
                elements.set(i, new Word(w.removeFirstCharDuplicates()));
            }
        }
    }
    public void reverseWords() {
        if (elements.size() > 1) {
            Element temp = elements.get(0);
            elements.set(0, elements.get(elements.size()-2));
            elements.set(elements.size()-2, temp);
        }
    }
    public Sentence removeSubstring(char firstChar, char lastChar) {
        String reg = firstChar + ".+" + lastChar;
        String result = toString().replaceAll(reg, "");
        Sentence s = new Sentence();
        s.parseSentence(result);
        return s;

    }
    public boolean findWord(Element word) {
            boolean noMatch = true;
            for (Element e2 : elements) {
                if(word instanceof Word && e2 instanceof Word){
                    if (word.toString().equals(e2.toString())) {
                        noMatch = false;
                    }
                }
            }
        return noMatch;
    }
    public Element getElement(int i){
        return elements.get(i);
    }
    public int getSize(){
        return elements.size();
    }
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder();
        for(Element w : elements){
           str.append(w.toString());
        }
        return str.toString();
    }


}
